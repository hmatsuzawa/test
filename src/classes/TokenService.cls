public class TokenService {
    
    public static String getToken() {
        String token = '';
        http h = new Http();
        Httprequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        req.setEndpoint('https://accounts.google.com/o/oauth2/token');
        req.setMethod('POST');
        req.setHeader('ContentType','application/x-www-form-urlencoded');
       
        String Header = '{"alg":"RS256","typ":"JWT"}';
        String Header_Encode = EncodingUtil.base64Encode(blob.valueof(Header));
        
        //CLINT ID
        //String claim_set = '{"iss":"517669177114-rkbht0u4bh97kt3m85i086eha5g60dvr.apps.googleusercontent.com"';
        String claim_set = '{"iss":"517669177114-rkbht0u4bh97kt3m85i086eha5g60dvr@developer.gserviceaccount.com"';
        
        claim_set += ',"scope":"https://www.googleapis.com/auth/fusiontables"';
        claim_set += ',"aud":"https://accounts.google.com/o/oauth2/token"';
        claim_set += ',"exp":"'+datetime.now().addHours(1).getTime()/1000;
        claim_set += '","iat":"'+datetime.now().getTime()/1000+'"}';
       
        String claim_set_Encode = EncodingUtil.base64Encode(blob.valueof(claim_set));
        String Singature_Encode = Header_Encode+'.'+claim_set_Encode;
        
        //genelateNew Json Key
        String key = '\nMIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAKy53A0cV4MWApFH\nZz3NtoNO+Z+DlY7vzEgPKAcWXqwn2PD8FchMWmDyNEx/LDiwzn1i42Ffz7JlAU/E\n7bvCjQ+968IXbGhXJ55fMLg+deYFKN9rMOZNq6N4mPoBFOVDAEVCfP1fgXoSP0+n\n4IFoz4u1fp3CfSf1R7ioPEJDAfBJAgMBAAECgYEAgZe1PXT9R9H0vTGYiWtVu7E+\nYuskBClw2MPeYWrsPzhqcWyBn1WCHei7SoXEFQi69KKEPWPYfl2AHWyAffZbRK+G\nFgYz+8x8KxkExq9rqOWxbF+W5RmijZEHT2qvtgDooliw2fMMhMhkswAyzIhJFxRS\nHtAe1Z3JQfPptgBSUjECQQDbPAykRpcXkunqPnxWrXknCLjBbb2RcjKcRqmPU7wo\n8hYaUf6aSDdpUhTz5VLk9riv2jFBz+2MVv74XsPB1W99AkEAybEnPisMumdC1/Cb\nIMOo6mEteRJJUaBzu0MVW5sum1nfU6JuGQGtt8hzJogc7XmKoZdkR9X9ZNFNJQuu\nDGz1vQJAXfXGlUGL0BavGi3drGh83DXouT4HWSbQehnu4fbJVeMrhexOamE5HnKH\neGB4vpjfXr7tDX/IF7XRYTF8RL7tIQJAHizX7zAnple+o2oG+37qba5+TCfVOlYk\nGnct4de0wF9Qz0V07V7X67Acsj4Y/dp6SDTLNwgn8gZyYUF/F7LRjQJBAL+Qaxmk\nnowJcjTXYsulbY9wTLCJSpx7iEftXcRo4J7QtyEY8VKZlk352d6fxOX7uLXTugJD\nAobBxBcZdPytmWo\u003d\n';
               
        blob privateKey = EncodingUtil.base64Decode(key);
        Singature_Encode = Singature_Encode.replaceAll('=','');
        String Singature_Encode_Url = EncodingUtil.urlEncode(Singature_Encode,'UTF-8');
        blob Signature_b =   blob.valueof(Singature_Encode_Url);
       
        String Sinatute_blob = base64(Crypto.sign('RSA-SHA256', Signature_b , privateKey));
        String JWT = Singature_Encode+'.'+Sinatute_blob;
        JWT = JWT.replaceAll('=','');
       
        String grt = 'urn:ietf:params:oauth:grant-type:jwt-bearer';
        req.setBody('grant_type='+grt+'&assertion='+JWT);
        res = h.send(req);
        if (res.getStatusCode() > 299) {
            system.debug(String.valueOf(res.getStatusCode()) + '-' + res.getStatus());
        } else {
            JSONParser parser = JSON.createParser(res.getBody());
            while (parser.nextToken() != null) {
                if (parser.getCurrentName() == 'access_token' && parser.nextValue() != null) {
                    token =  parser.getText();
                    break;
                }
            }
        }
        return token;
    }
    
     public static String base64(Blob b) {
        String ret = EncodingUtil.base64Encode(b);
        ret = ret.replaceAll('\\+', '-');
        ret = ret.replaceAll('/', '_');
        ret = ret.replaceAll('=', '');
        return ret;
    }
}