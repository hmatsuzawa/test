public class FusionTableExample {
    public String tableId {get;set;}
    public Integer statusCode {get;set;}
    public String res_str {get;set;}
    public String access_token {get;private set;}
    public List<Account> accountList {get;private set;}
    
    public FusionTableExample() {
        getToken();
        accountList =  [select name,BillingPostalCode,AnnualRevenue,NumberOfEmployees from account limit 2];
    }
    
    public void getToken() {
        access_token = TokenService.getToken();
    }
    
    public void insertRow() {
        String bodyString = '';
        for (account item : accountList){
            bodyString += '"'+item.name +'","'+item.BillingPostalCode + '","'+item.AnnualRevenue +'","'+
                item.NumberOfEmployees + '"\n';
        }
        bodyString = bodyString.removeEnd('\n');
        //FusionTableId
        tableId = '1ZZMzD50XwN96lM4Q3JC-rOUCAWzldWYOrbzSf8Ou';
        //string sessToken = ApexPages.currentPage().getParameters().get('token'); key=
        String endPoint = 'https://www.googleapis.com/upload/fusiontables/v1/tables/'+tableId+'/import?key=AIzaSyD_15PEOEmX-oBuCQOf1pJeZm2JSOuv4CY';
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPoint);
        req.setHeader('Authorization', 'Bearer ' + access_token );
        req.setHeader('content-type', 'application/octet-stream');
        req.setHeader('Content-length',string.valueof(bodyString.length()));
        req.setHeader('Uploadtype', 'media' );
        req.setHeader('charset','utf-8');
        req.setMethod('POST');
        req.setBody(bodyString);
        system.debug('★'+bodyString);
        
        try {
            Http h = new Http();
            HttpResponse res = h.send(req);
            if (res.getStatusCode() > 299) {
                        system.debug(String.valueOf(res.getStatusCode()) + '-' + res.getStatus());
            }
            statusCode = res.getStatusCode();
            res_str = res.getBody();
            System.debug('res_str: '+ res_str);
        } catch( System.Exception e) {
            System.debug('ERROR: '+ e);
        }
    }
}