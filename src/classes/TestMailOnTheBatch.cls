global class TestMailOnTheBatch implements Database.Batchable<sobject>{

	global Database.Querylocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator([select id,name from Opportunity]);
	}

	global void execute(Database.BatchableContext BC,List<Opportunity> opp) {
		try {
			List<Messaging.SingleEmailmessage> messeList = new List<Messaging.SingleEmailmessage>();

			Messaging.SingleEmailmessage message = new Messaging.Singleemailmessage();
			List<String> to = new List<String>();
			for (integer i = 0;i < 10; i++) {
				to.add('sdfcma9imvaodskv@yahoo.co.jp');
			}
			message.setToAddresses(to);
			message.setReplyTo('toshikazu03020213@yahoo.co.jp');
			message.setSenderDisplayName('松沢');
			message.setSubject('テストです');
			message.setPlainTextBody('aiueo \n aaaaa');
			message.setUseSignature(false);

			messeList.add(message);

			Messaging.sendEMail(messeList);
		} catch(Exception e) {
			system.debug('■'+e);
		}
	}

	global void finish(Database.BatchableContext BC) {

	}
}