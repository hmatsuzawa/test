public with sharing class SearchEngineerContractController {

    public List<ContractAdmin__c> contAdminList {get;set;}
    public ContractAdmin__c inputAdmin {get;set;}
    public boolean viewFlg {get;set;}
    
    public SearchEngineerContractController () {
        this.contAdminList  = new List<ContractAdmin__c>();
        this.inputAdmin = new ContractAdmin__c();
        this.viewFlg = false;
    }
    
    public void SearchContAdmin() {
        string queryString = 'select id,Name,Engineer__c,ContractStartDate__c,ContractCompletedDate__c,ContractNow__c,Type__c,DocumentName__c,Status__c,ExecuteDate__c from ContractAdmin__c';
        string condtion = ' where ';
        
        if (inputAdmin.Type__c != null) {
            queryString  += condtion + 'Type__c = \'' + inputAdmin.Type__c + '\'';
            condtion = ' and ';
        }
        
         if (inputAdmin.Status__c!= null) {
            queryString  += condtion + 'Status__c = \'' + inputAdmin.Status__c+ '\'';
            condtion = ' and ';
        }
        
        this.contAdminList = database.query(queryString);
        if (this.contAdminList.size() > 0){
             this.viewFlg = true;
        }
   
    }
}