public with sharing class squrityTest {
    
    public MusicBand__c music {get;set;}
    public String text {get;set;}
    
    public squrityTest(ApexPages.StandardController con){
      this.music = [select id,name,vocal__c from MusicBand__c limit 1];
      system.debug('@@@'+music.vocal__c );
    }
    
    public void save() {
       
        this.music.vocal__c = this.text;
        update this.music;

    }
    
}