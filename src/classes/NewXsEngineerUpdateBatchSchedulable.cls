global class NewXsEngineerUpdateBatchSchedulable implements Schedulable{

  private integer startHours;

  public NewXsEngineerUpdateBatchSchedulable(integer i) {
    this.startHours = i;
    system.debug('★★');
  }

  public NewXsEngineerUpdateBatchSchedulable() {
       system.debug('■■');
  }

  global void execute(SchedulableContext BC) {
    if(this.startHours == null) {
        system.debug('■null■');
    } else {
        system.debug('■this.startHours■');
    }
  }

}