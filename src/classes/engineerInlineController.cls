public with sharing class engineerInlineController {
    public skill__c skillItems {get;set;}
    public String Experience {get;set;}
    public String engineerId {get;set;}
    public List<Skill__c> skillList {get;set;}
    public String optionVal {get;set;}
    public string engineerName {get;set;}
    public boolean skillListViewFlg {get;set;}
    public string hiddenVal {get;set;}
    public List<innerClass> innerList {get;set;}
    public String lineNumber {get;set;}
    public List<innerClass> innerList2 {get;set;}
    public String leftFlg {get;set;}
    public date testDate {get;set;}
    public Engineer__c  eng {get;set;}
    public List<innerSkillClass> inSkillList {get;set;}

    private final string NO_DATA = '--なし--';

    /**
     *  innerClass()
     *  画面上部表示用インナークラス
     *
    **/
    public class innerClass {
        public List<Selectoption> typeList {get;set;}
        public String typeVal {get;set;}
        public String typeViewVal {get;set;}
        public String ExperienceVal {get;set;}
        public Skill__c viewSkill {get;set;}

    }

    /**
     *  innerClass()
     *  スキル表示用インナークラス
     *
    **/
    public class innerSkillClass {
        public Skill__c viewSkill {get;set;}
        public String experienceVal {get;set;}
        public List<Selectoption> experienceList {get;set;}
        public String saveFlg {get;set;}
    }

    /**
     *  engineerInlineController()
     *  コンストラクタ
     *
    **/
    public engineerInlineController (ApexPages.StandardController controller) {
        this.eng = new Engineer__C();
        this.skillItems = new Skill__c();

        this.engineerId = ApexPages.currentPage().getParameters().get('id');

        this.innerList = new List<innerClass>();
        this.innerList2 = new List<innerClass>();

        // 属性List
        List<aggregateresult> tpList = [
            select
                SkillType__c
            from
                language__c
            group by
                SkillType__c
        ];

        Map <String,List<language__c>> langMap = new Map<String,List<language__c>>();

        List<language__c> langList = [
            select
                name,
                TypeSkillCode__c,
                SkillType__c,
                SkillCode__c,
                SkillTypeCode__c
            from
                language__c
            order by
                Name
        ];

        for (language__c lang : langList) {

            if (langMap.containsKey(lang.SkillType__c)) {
                langMap.get(lang.SkillType__c).add(lang);
            } else {
                List<language__c> langlang = new List<language__c>();
                langlang.add(lang);
                langMap.put(lang.SkillType__c,langlang);
            }
        }

        boolean listAddFlg = false;
        for (aggregateResult agg : tpList) {

            innerClass inClass = new innerClass();

            // 言語
            inClass.typeList = new List<Selectoption>();
            inClass.typeList.add(new selectoption(NO_DATA,NO_DATA));

            for (language__c lang : langMap.get(string.valueof(agg.get('SkillType__c')))) {
                inClass.typeList.add(new selectoption(lang.TypeSkillCode__c,lang.name));
            }

            //経験年数のselectList用
            inClass.viewSkill = new Skill__c();

            // 表示名
            inClass.typeViewVal = string.valueof(agg.get('SkillType__c'));

            if (!listAddFlg) {
                this.innerList.add(inClass);
                listAddFlg = true;
            } else {
                this.innerList2.add(inClass);
                listAddFlg = false;
            }
        }

        getSkills();

        if (this.inSkillList.size() > 0) {
            this.skillListViewFlg= true;
        } else {
            this.skillListViewFlg= false;
        }
    }

    /**
     *  skillDelete()
     *  削除ロジック
     *
    **/
    public void skillDelete() {
        try {
            Skill__c skillItem = [
                select
                    id
                from
                    skill__c
                where
                    id =: this.hiddenVal
            ];

            delete skillItem;

            getSkills();

            if(this.inSkillList.size() == 0){
                 this.skillListViewFlg= false;
            }
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'エラーが発生しました。リロードしてください。'));
        }
    }

    /**
     *  getSkills()
     *
     *
    **/
    private void getSkills() {

        this.skillList = [
            select
                id,
                EngineerId__c,
                Experience__c,
                SkillMasterId__r.Name,
                SkillMasterId__r.SkillType__c,
                EngineerIdMasterCord__c,
                isGoodAt__c
            from
                Skill__c
            where
                EngineerId__c =: this.engineerId
            order by
                isGoodAt__c desc,SkillMasterId__r.SkillType__c,SkillMasterId__r.Name
        ];

        this.inSkillList = new List<innerSkillClass>();

        // Skill__cから選択リスト値取得
        Schema.DescribeFieldResult F = Skill__c.Experience__c.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        List<Selectoption> pickValues = new List<Selectoption>();
        for (PicklistEntry i : p){
            pickValues.add(new SelectOption(i.getlabel(),i.getlabel()));
        }
        for (Skill__c skillItem : this.skillList) {
            innerSkillClass inSkill = new innerSkillClass();
            inSkill.viewSkill = skillItem;
            inSkill.saveFlg = 'false';
            inSkill.experienceList = pickValues;
            inSkill.experienceVal = skillItem.Experience__c;
            this.inSkillList.add(inSkill);
        }
    }

    /**
     *  save()
     *  保存ロジック
     *
    **/
    public void save() {

        try {
            List<skill__c> saveTarget = new List<skill__c>();
            Integer lineNo = integer.valueOf(this.lineNumber);

            innerClass inItem;

            if (this.leftFlg == 'true') {
                inItem = this.innerList.get(lineNo);
            } else {
                inItem = this.innerList2.get(lineNo);
            }

            string upsertKey = this.engineerId + inItem.typeVal;

            language__c lang = [
                select
                    id
                from
                    language__c
                where
                    TypeSkillCode__c =: inItem.typeVal
            ];

            skill__c skill = new Skill__c(
                EngineerId__c = (id)this.engineerId,
                name = this.engineerId,
                Experience__c = inItem.viewSkill.Experience__c,
                EngineerIdMasterCord__c = upsertKey,
                SkillMasterId__c = lang.id
            );

            this.skillListViewFlg= true;
            upsert skill EngineerIdMasterCord__c;

            getSkills();
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'エラーが発生しました。リロードしてください。'));
        }
    }

    /**
     *  editSave()
     *  年数編集保存
     *
    **/
    public pagereference editSave() {
        try {
            List<Skill__c> saveTargetList = new List<Skill__c>();
            for (innerSkillClass skillItem : this.inSkillList) {
                if (skillItem.saveFlg == 'true') {
                    skillItem.viewSkill.Experience__c = skillItem.experienceVal;
                    saveTargetList.add(skillItem.viewSkill);
                }
                skillItem.saveFlg = 'false';
            }

            if (saveTargetList.size() > 0) {
                upsert saveTargetList EngineerIdMasterCord__c;
            }

            getSkills();
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'エラーが発生しました。リロードしてください。'));
        }
        return null;
    }

}