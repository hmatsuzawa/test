public with sharing class ExplainQueryController {

    private String instance;
    transient public String soql { get; set; }
    transient public String retJSON { get; set; }
    public List<Plan> plans { get; set; }

    public ExplainQueryController() {
        instance = 'https://' + URL.getSalesforceBaseUrl().getHost().replace('.visual.force.com', '.salesforce.com').replace('c.','');
    }    
    
    public PageReference showExplainPlan() {
       try {
        HTTPRequest req = new HTTPRequest();
        req.setHeader('Authorization','Bearer ' + UserInfo.getSessionID());
        req.setHeader('Content-Type','application/json');
        req.setEndpoint(instance + '/services/data/v30.0/query/?explain=' + EncodingUtil.urlEncode(soql, 'UTF-8'));
        req.setMethod('GET');
        
        Http h = new Http();
        HttpResponse res = h.send(req);
        String status = res.getStatus();
        Integer statusCode = res.getStatusCode();
        retJSON = res.getBody();
        plans = new List<Plan>();
      
 
        if (statusCode != 200) {
            // Error
            for(ExplainErrorResponse error : (List<ExplainErrorResponse>) JSON.deserialize(retJSON, List<ExplainErrorResponse>.class)){ 
                String err = error.errorCode +' '+ error.message;
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, status + ' - ' + err);
                ApexPages.addMessage(msg);
            }
        } else { 
                    
            ExplainResponse response = (ExplainResponse) JSON.deserialize(retJSON, ExplainResponse.class);
            plans = response.plans;     
        }    
         } catch(Exception e) {
              ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
                ApexPages.addMessage(msg);
          }
        return null;
    }

    public class ExplainErrorResponse{
        String errorCode {get; set;}
        String message {get; set;}
    }
    
    public class ExplainResponse{
        public List<Plan> plans {get; set;}
    }
    
    public class Plan {
        public Double cardinality { get; set; }
        public List<String> fields { get; set; }
        public String leadingOperationType { get; set; }
        public Double relativeCost { get; set; }
        public Double sObjectCardinality { get; set; }
        public String sObjectType { get; set;}
    }
        
}