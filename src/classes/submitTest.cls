public class submitTest {

    public boolean boxItem{get;set;}
    public musicband__c musicItem {get;set;}
    public String textItem { get; set; }
    public List<musicband__c> musicList {get;set;}
    public boolean errorFlg {Get;set;}
      public String text {get;set;}
      
    public submitTest(){
        musicItem = [
            select
                checkBox__c,
                Vocal__c
            from 
                musicband__c
            limit 1
        ];
        
        musicList = [
            select
                checkBox__c,
                Vocal__c
            from 
                musicband__c
        ];
        
        
    }
    
    public pagereference save(){
        this.errorFlg = true;
        this.text = 'text';
        system.debug('aaaaa+++++');
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'RERENDER_COMPLERE');
         ApexPages.addMessage(myMsg);
        return null;
    }
}