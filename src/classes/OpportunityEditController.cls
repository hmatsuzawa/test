public with sharing class OpportunityEditController {

    public String name {get;set;}
    
    public Opportunity opp {get;set;}
    
    public OpportunityEditController(){
        opp = new Opportunity();
        
    }
    
    public PageReference save(){
        //OpportunityEditController oe = new OpportunityEditController();
        //Date compDay = new Date();
        //PageReference pageRef　= new PageReference('http://www.google.com');
        PageReference pageRef = ApexPages.currentPage();
        opp.Name = name;
        opp.StageName = 'インディーズ';
        opp.CloseDate = Date.parse('2009/09/09'); 
        insert opp;
        return pageRef;
    }
}