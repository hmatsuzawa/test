global with sharing class getIndex2 {
    
    public List<selectoption> selectList {get;set;}
    public List<selectoption> selectJapList {get;set;}
    public String optionVal {get;set;}
    public String reVal {get;set;}
   // public List<viewVal> viewList {get;set;}
    public viewVal objectInfo {get;set;}
       
    /*public getIndex() {
        Map<String, Schema.SObjectType> sMap = Schema.getGlobalDescribe();

        this.selectList = new List<selectoption>();
        this.selectJapList = new List<selectoption>();
        
        for (String objName : smap.keySet()) {
           string objLabel = sMap.get(objName).getDescribe().getLabel();
           this.selectList.add(new Selectoption(objName,objName + '：' + objLabel));
           this.selectJapList.add(new Selectoption(objName, objLabel+ '：' + objName));
        }
        
        this.objectInfo = new viewVal(
        isCustom(String.valueof(result.iscustom())),
        result.getName(),
        result.getLabel(),
        '',
        ''
        );
        this.selectList.sort();
        this.selectJapList.sort();
   
    }*/
    
    @RemoteAction   
    global static List<viewVal> getField() {
        String param = System.currentPageReference().getParameters().get('Opportunity');
        Map<String, Schema.SObjectType> sMap = Schema.getGlobalDescribe();
        Schema.SObjectType sObj = sMap.get(param);
        Schema.DescribeSObjectResult result = sObj.getDescribe();
        Map<String, Schema.SObjectField> fmap = result.fields.getMap();

         List<viewVal> viewList = new List<viewVal>();
        // 標準項目が上
        for (String str : fmap.KeySet()) {
             Schema.SObjectField f = fmap.get(str);
            Schema.DescribeFieldResult fr = f.getDescribe();
            if (!fr.iscustom()) {
                 viewVal viewItem = new viewVal(
                 isCustom(String.valueof(fr.iscustom())),
                 fr.getName(),
                 fr.getLabel(),
                 String.valueof(fr.getType()),
                 String.valueof(fr.getLength())
                 );
                 viewList.add(viewItem);
             }
         }
         
         //カスタム項目が下
         for (String str : fmap.KeySet()) {
             Schema.SObjectField f = fmap.get(str);
             Schema.DescribeFieldResult fr = f.getDescribe();
            if (fr.iscustom()) {     
                 viewVal viewItem = new viewVal(
                 isCustom(String.valueof(fr.iscustom())),
                 fr.getName(),
                 fr.getLabel(),
                 isReference(fr),
                 String.valueof(fr.getLength())
                 );
                viewList.add(viewItem);
             }
         }
         
         return viewList;
    }
    
    global without sharing class viewVal {
        public string name {get;set;}
        public string label {get;set;}
        public string type {get;set;}
        public string length {get;set;}
        public string isCustom {get;set;}
        public viewVal(String isCustom,String name,String label,String type,String length) {
            this.isCustom = iscustom;
            this.name = name;
            this.label = label;
            this.type = type;
            this.length = length;
        }
    }
    
    global static string isCustom(String key) {    
       if (key == 'true') {
           return 'カスタム';
       } else {
           return '標準';
       }
    }
    
   global static string isReference(Schema.DescribeFieldResult key) {
         string stringKey = string.valueof(key.getType());
         if (stringKey == 'REFERENCE') {
           return stringKey  +' ： ' +key.getRelationshipName();
       } else {
           return stringKey;
       }
    }
}