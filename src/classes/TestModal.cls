public class TestModal {

	public CustomObject0__c yuq {get;set;}

	public List<String> honbunList {get;set;}

	public String kigyou {get;set;}
	public String ue {get;set;}
	public String hedder {get;set;}
	public String kahen {get;set;}
	public String free {get;set;}
	public string honbun {get;set;}
	public String futter {get;set;}
	public String syomei {get;set;}
	public String hasen {get;set;}
	public String subject {get;set;}
	public String viewHonbun {get;set;}
	public String to {get;set;}
	public String cc {get;set;}
	public String tempCondition {get;set;}
	public String mailFrom {get;set;}
	public boolean deleteFlg {get;set;}

	public TestModal() {
		String yuqId = ApexPages.currentPage().getParameters().get('id');
		List<CustomObject0__c> mailList= [
			select
				id,
				hedder__c,
				hedder__r.bunsyou__c,
				honbun__c,
				honbun__r.bunsyou__c,
				futter__c,
				futter__r.bunsyou__c,
				User__r.syomei__c,
				Subject__c,
				To__c,
				CC__c,
				User__c,
				User__r.Email,
				free1__c,
				free2__c,
				free3__c,
				ue__c,
				ue__r.bunsyou__c
			from
				CustomObject0__c
			/*where
				ID__c =: yuqId*/
		];

		tenpre__c temp = [
			select
				bunsyou__c
			from
				tenpre__c
			where
				bunrui__C = 'その他'
		];

		this.hasen = '\r\n\r\n' + temp.bunsyou__c + '\r\n\r\n';

		if (mailList.size() > 0) {

			this.yuq = mailList.get(0);

			this.kigyou = checkVal(this.yuq.free1__c);
			this.kahen = checkVal(this.yuq.free2__c);
			this.free = checkVal(this.yuq.free3__c);

			this.to = checkVal(this.yuq.to__c);
			this.cc = checkVal(this.yuq.cc__c);
			this.mailfrom = checkVal(this.yuq.User__r.Email);
			if (this.mailfrom != null && this.mailfrom != '') {
				if (this.cc != null && this.cc != '') {
					this.cc += ',' + this.mailfrom;
				} else {
					this.cc = this.mailfrom;
				}
			}
			this.subject = checkVal(this.yuq.subject__c);

			this.ue = checkVal(this.yuq.ue__r.bunsyou__c);
			this.hedder = checkVal(this.yuq.hedder__r.bunsyou__c);
			this.honbun = checkVal(this.yuq.honbun__r.bunsyou__c);
			this.futter = checkVal(this.yuq.futter__r.bunsyou__c);
			this.syomei = this.yuq.User__r.syomei__c;

			createHonbun();
			this.deleteflg = true;
			system.debug('■'+honbun );
		} else {
			this.yuq = new CustomObject0__c();
			//this.yuq.ID__c = yuqId;
			this.kigyou = '';
			this.kahen = '';
			this.free = '';

			this.to = '';
			this.cc = '';
			this.mailfrom = '';

			this.subject = '';

			this.ue = '';
			this.hedder = '';
			this.honbun = '';
			this.futter = '';
			this.syomei = '';
			this.deleteflg = false;
		}

	}

	public void changefree1() {

		this.kigyou = this.yuq.free1__c;
		createHonbun();
	}

	public void changefree2() {

		this.kahen = this.yuq.free2__c;
		createHonbun();
	}

	public void changefree3() {

		this.free = this.yuq.free3__c;
		createHonbun();
	}

	public void changeSubject() {

		this.subject = this.yuq.Subject__c;
		createHonbun();
	}

	public void changeTo() {

		this.to = this.tempCondition;
		createHonbun();
	}

	public void changeCc() {
		this.cc = this.tempCondition;
		if (this.tempCondition != null &&  this.tempCondition != '' && this.mailFrom != null && this.mailFrom != '') {
			this.cc = this.tempCondition + ',' + this.mailFrom;
		} else if(this.tempCondition != null &&  this.tempCondition != '') {
			this.cc = this.tempCondition;
		} else if(this.mailFrom != null &&  this.mailFrom != '') {
			this.cc = this.mailFrom;
		} else {
			this.cc = '';
		}
		createHonbun();
	}

	public void changeUe() {

		List<tenpre__c> temp = getTemp(this.yuq.ue__c);
		if (temp.size() > 0) {
			this.ue = temp[0].bunsyou__c;
		} else {
			this.ue = '';
		}
		createHonbun();
	}

	public void changeHedder() {

		List<tenpre__c> temp = getTemp(this.yuq.hedder__c);
		if (temp.size() > 0) {
			this.hedder = temp[0].bunsyou__c;
		} else {
			this.hedder = '';
		}
		createHonbun();
	}

	public void changeHonbun() {

		List<tenpre__c> temp = getTemp(this.yuq.honbun__c);
		if (temp.size() > 0) {
			this.honbun = temp[0].bunsyou__c;
		} else {
			this.honbun = '';
		}
		createHonbun();
	}

	public void changeFutter() {

		List<tenpre__c> temp = getTemp(this.yuq.futter__c);
		if (temp.size() > 0) {
			this.futter = temp[0].bunsyou__c;
		} else {
			this.futter = '';
		}
		createHonbun();
	}

	public List<tenpre__c> getTemp(String condition) {
		List<tenpre__c> temp = [
			select
				bunsyou__c
			from
				tenpre__c
			where
				id = :condition
		];

		return temp;
	}

	public void changeFrom() {

		if(this.yuq.User__c != null) {
			User user = [
				select
					syomei__c,
					Email
				from
					User
				where
					id = :this.yuq.User__c
			];
			this.mailFrom = user.Email;
			this.futter = user.syomei__c;
			if (this.cc != null && this.cc != '') {
				this.cc += ',' +  user.Email;
			} else {
				this.cc = user.Email;
			}
		} else {
			this.mailFrom = '';
			this.futter = '';
			if (this.yuq.cc__c != null && this.yuq.cc__c != '') {
				this.cc = this.yuq.cc__c;
			} else {
				this.cc  = '';
			}
		}
		createHonbun();
	}

	public void save() {
		if (this.yuq.id != null) {
			try {

				update this.yuq;
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM,'メール内容を更新しました'));
			} catch (exception e) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'メール内容を更新に失敗しました'));
			}
		} else {

			try {
				insert yuq;
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM,'メールを新規作成しました'));
			} catch(exception e) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'メーの新規作成に失敗しました'));
			}

		}
	}

	public void remove() {
		if (this.yuq.id != null) {
			delete this.yuq;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM,'メールを削除しました'));
		} else {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'メールは存在しません'));
		}
	}

	public void createHonbun() {
		String rep = '';
		if (this.kigyou != null && this.kigyou != '') {
			rep += this.kigyou + '\r\n\r\n';
		}
		system.debug('■' + rep);
		rep += this.ue + '\r\n\r\n';

		if (this.free != null && this.free != '') {
			rep += free + '\r\n\r\n';
		}

		if (this.hedder != null && this.hedder != '') {
			rep += this.hedder;
		}

		rep += this.hasen + this.honbun +  this.hasen + this.futter + '\r\n\r\n' +this.syomei;

		if (this.kahen != null && this.kahen != '') {
			rep = rep.replaceAll('@@1@@',kahen);
		}
		system.debug('★'+rep);
		this.viewHonbun = rep.replaceAll('\r\n','<br/>');
		system.debug('★'+this.viewHonbun);
	}

	private String checkVal(String val) {
		if (val == null) {
			return '';
		} else {
			return val;
		}
	}

	public void sendingMail() {
		String rep = '';
		if (this.kigyou != null && this.kigyou != '') {
			rep += this.kigyou + '\r\n\r\n';
		}
		system.debug('■' + rep);
		rep += this.ue + '\r\n\r\n';

		if (this.free != null && this.free != '') {
			rep += free + '\r\n\r\n';
		}

		if (this.hedder != null && this.hedder != '') {
			rep += this.hedder;
		}

		rep += this.hasen + this.honbun +  this.hasen + this.futter + '\r\n\r\n' +this.syomei;

		if (this.kahen != null && this.kahen != '') {
			rep = rep.replaceAll('@@1@@',kahen);
		}

		List<Messaging.SingleEmailmessage> messeList = new List<Messaging.SingleEmailmessage>();
		Messaging.SingleEmailmessage message = new Messaging.Singleemailmessage();
		List<String> toList = new List<String>{this.to};
		List<String> ccList = this.cc.split(',');

		message.setToAddresses(toList);
		message.setCcAddresses(ccList);
		message.setReplyTo(this.mailFrom);
		//message.setSenderDisplayName('松沢');
		message.setSubject(this.subject);
		message.setPlainTextBody(rep);
		message.setUseSignature(false);
		messeList.add(message);

		Messaging.sendEMail(messeList);
	}
}