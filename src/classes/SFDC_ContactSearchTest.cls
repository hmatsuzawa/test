public with sharing class SFDC_ContactSearchTest {
   
    public List<hoge> hogeList {get;set;}
    public String selectedTab { get; set; }
    public static final String backgroundYellow ='background-color:#ffff66';
    public List<Id> selectedList {get; set;}
    public String backgroundColor { get; set; }
    public boolean Selected { get; set; }
        
    public Class hoge {
        public String backgroundColor { get; set; }
        public boolean Selected { get; set; }
        public Contact DispContact{ get;set;}
        public hoge(){}
    }
    
    public SFDC_ContactSearchTest(){
       selectedTab = 'Consignment';
       this.selectedList = new List<Id>();
       this.hogeList= new List<hoge>();
    } 
    
    public String getContactList() {
        return null;
    } 
    
    public void defaultSearch(){
        hogeList = new List<hoge>();
         List<contact> conList = [
            select
                id,
                name,
                AccountID
            from
                contact
            order by name desc
            limit 10
         ];
         for(contact con :conList ) {
             hoge ho = new hoge();
             ho.DispContact = con;
             hogeList.add(ho);
         }
    }
    public PageReference doSearch() {
        hogeList = new List<hoge>();
         List<contact> conList = new List<contact>();
        if(selectedTab == 'Contact'){
             conList = [
                select
                    id,
                    name,
                    AccountID
                from
                    contact
                order by name asc 
                limit 5
             ];
        } else if(selectedTab == 'Consignment'){
            conList = [
                select
                    id,
                    name,
                    AccountID
                from
                    contact
                order by name desc
                limit 10
             ];
        }
        for(contact con :conList) {
             hoge ho = new hoge();
             ho.DispContact = con;
             hogeList.add(ho);
         }
        return null;
    }
    
    public PageReference selected() {
        List<Id> resultList = new List<Id>();
        Contact resultItem = null;
        Integer recordsCnt = 0;
        for(Integer i = 0; i < this.hogeList.size(); i++) {
            SFDC_ContactSearchTest.hoge records = this.hogeList.get(i);
            if(records.Selected){
                recordsCnt++;
                resultItem = records.DispContact;
                resultList.add(resultItem.Id);
            }   
        }
        if (resultList.size() != 0) {
            if (selectedList.size() == 0) {
                // ユーザIDをメモリ上に保持していない場合
                for(Integer i = 0; i < resultList.size(); i++) {
                    selectedList.add(resultList.get(i));
                }
            } else {
                // ユーザIDをメモリ上に保持している場合
                List<Id> tmpList = new List<Id>();
                Boolean findFlg = false;
                for(Integer i = 0; i < resultList.size(); i++){
                    findFlg = false;                        
                    for(Integer j = 0; j < selectedList.size(); j++){
                        if (resultList.get(i) == selectedList.get(j)) {
                            findFlg = true;
                            break;
                        }
                    }
                    if (!findFlg) {
                        tmpList.add(resultList.get(i));
                    }
                }
                if (tmpList.size() != 0) {
                    selectedList.addAll(tmpList);
                }
            }
        }
        setColor();
        return null;
    }

    public void setColor() {
        String colorString = backgroundYellow; 
        for (Integer i = 0; i < this.selectedList.size(); i++){
            if (this.hogeList != null) {
                for (Integer j = 0; j < this.hogeList.size(); j++){
                    if (this.selectedList.get(i) == this.hogeList.get(j).DispContact.Id) {
                        this.hogeList.get(j).Selected = false;
                        this.hogeList.get(j).backgroundColor = colorString;
                        break;
                    }
                }
            }
        }
    }

}