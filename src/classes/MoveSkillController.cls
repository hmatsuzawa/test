public with sharing class MoveSkillController {
    string engineerId  {get;set;}
    
    public MoveSkillController(ApexPages.StandardController controller) {
        this.engineerId = controller.getId();
        
    }
    
    public pagereference movePage(String engineerId) {
        Pagereference page = new Pagereference('SkillDetailPage');
        page.getparameters().put('id',engineerId);
        return page;
    }
    

}