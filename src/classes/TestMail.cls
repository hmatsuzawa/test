public with sharing class TestMail {

	public List<Messaging.Singleemailmessage> messages;

	public TestMail() {
		this.messages = new List<Messaging.Singleemailmessage>();
	}

	// replyTo 返信先
	// senerDisplayName :差出人名
	// toAddress :to
	// toAddress :cc
	// subJect:件名
	// planeTest:本文
	public void createSendMail(String replyTo,String senerDisplayName,List<String> toAddress,List<String> ccAddress,
	String subJect,String planeText) {


		try {

			if (toAddress != null && toAddress.size() > 0) {

				Messaging.SingleEmailmessage message = new Messaging.Singleemailmessage();

				message.setToAddresses(toAddress);

				if (replyTo != null) {
					message.setReplyTo(replyTo);
				}

				if (senerDisplayName != null) {
					message.setSenderDisplayName(senerDisplayName);
				}

				if (ccAddress != null && ccAddress.size() > 0) {
					message.setCcAddresses(ccAddress);
				}

				if (subJect != null) {
					message.setSubject(subJect);
				}

				if (planeText != null ) {
					message.setPlainTextBody(planeText);
				}
				//String id = '00XG0000001L9xI';
				//message.setTemplateId((Id)id);
				message.setUseSignature(false);
				this.messages.add(message);

			} else {

			}
		} catch (Exception e) {
			throw e;
		}
	}

	public void send() {
		if (this.messages.size() > 0) {
			 Messaging.sendEmail(messages);
		}
	}
}