@isTest
private class TestSchTest{

    static testMethod void myUnitTest() {
        Test.StartTest();
        TestBatchSchedule ss = new TestBatchSchedule();
        String dateCron = Datetime.now().addSeconds(5).format('s m H d M ? yyyy');
        String jobId = System.schedule('testBasicScheduledApex',dateCron, ss);
        Test.StopTest();
    }

}