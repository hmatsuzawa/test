public class blobLimitStab {
    public transient String blobName {get;set;}
    public transient blob blobBody {get;set;}
    public boolean errorFlg {get;set;}
    public boolean attachCheck {get;set;}
    public blobLimitStab() {
        String errorFlg = ApexPages.currentPage().getParameters().get('errorFlg');
        if (errorFlg == 'true') {
            this.errorFlg = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '添付ファイルのサイズが1MBより大きいです。'));
        }
    }
    // akucyon
    public pagereference actionMethod() {
        system.debug('★blobName★' + blobName );
         system.debug('★blobBody★' + blobBody );
        if (this.blobName != null) {
            if (this.blobBody == null) {
                 system.debug('■blob null■');
                 return errorProccess();
            }
            //サイズが1MB以下なら添付作成
            if (this.blobBody.size() <= 1048576) {
                system.debug('■'+this.blobBody.size());
                attachment attachItem = createAttachment();
                //作成処理はattachMentのparentIdに設定するものがないのでコメントアウト
                /* Database.SaveResult result = Database.insert(attachItem);
                
                if (result.isSuccess()) {
                    // 成功するとカスタムオブジェクトの項目更新処理
                } else {
                    return errorProccess();
                }*/
            } else {
                system.debug('■'+this.blobBody.size());
                return errorProccess();
            }
        }
        //添付がある状態でメール送信する場合と、添付がない場合のメール送信の２パターンが
        //存在します
        PageReference retPage = new PageReference('/apex/blobLimitStabPage');
        return retPage;
    }
            
    private attachment createAttachment(){
        attachment attach = new attachment();
        attach.name = this.blobName;
        attach.body = this.blobBody;
        return attach;
    }
    
    private PageReference errorProccess(){
        
        // 1MBより大きい場合、自画面に遷移し、エラー表示をさせる
        // この際、前画面で受け取ったパラメータを再設定する。
        PageReference retPage = new PageReference('/apex/blobLimitStabPage');
        retpage.getParameters().put('errorFlg','true');
        retpage.setRedirect(true);
        return retPage;
    }
}