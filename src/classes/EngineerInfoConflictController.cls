public with sharing class EngineerInfoConflictController {
    public boolean viewButtonFlg {get;set;}
    
    public EngineerInfoConflictController (ApexPages.StandardController controller) {
        
        String engineerId = Apexpages.CurrentPage().getParameters().get('id');
        
        Engineer__c engineer = [select isConflict__c from engineer__c where id =:engineerId];
        
        if (engineer.isConflict__c) {
            viewButtonFlg = true;
        } else {
           viewButtonFlg = false;
        }
   }
}