global class TestBatch implements Database.batchable<sObject>, Database.stateful {

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([
            SELECT 
                id
            FROM
                opportunity 
        ]);
    }

    //バッチ実行処理
    global void execute(Database.BatchableContext BC, List<sObject> scope){
      
    }

    global void finish(Database.BatchableContext BC){
        system.debug('finish');
    }
}