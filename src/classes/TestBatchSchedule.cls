global without sharing class TestBatchSchedule implements Schedulable{
    
    
    global void execute(SchedulableContext sc) {
            // バッチを実行
            Database.executeBatch(new TestBatch());
    }
}