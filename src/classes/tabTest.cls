public class tabTest {
    public List<opportunity> oppList {get;set;}
    public list<user> userList {get;set;}
    public List<contact> contactList {get;set;}
    public list<account> accList {get;set;}
    public String selectedTab {get;set;}
    public account mainAccount {get;set;}
    public Skill__c skillItem {get;set;}
    public tabtest(){
    this.oppList = [select id,name from opportunity limit 1]; 
    this.userList = [select id,name from user limit 1]; 
    this.contactList = [select id,name from contact  limit 1]; 
    this.accList = [select id,name from account limit 1]; 
    this.mainAccount = [select id,name from account order by id desc limit 1]; 
    }
    
    public void changeOppData() {
    this.oppList = [select id,name from opportunity limit 10];
    selectedTab = 'opp';
    }
    
    public void changeContactData() {
     this.contactList = [select id,name from contact limit 10];
     selectedTab = 'contact';
    }
}