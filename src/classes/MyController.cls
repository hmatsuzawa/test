public class MyController {

    public PageReference save() {
        return null;
    }

    public String getName() {
        return 'MyController';
    }

    public Account getAccount() {
        return [SELECT Id, Name FROM Account 
                WHERE Id = :System.currentPageReference().getParameters().get('id')]; 
    } 
}